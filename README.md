# MadgraphBatch

This package uses stand-alone Madgraph (not included in ATLAS software) to produce events on theta. The project essentially consists of two scripts: generaty.py and copyMGOutDir.py.

The generate.py fetches a physics process from the the processes.procDict dictionary and generates the diagram for this process with the MadGraph mg5_aMC command. The output directory of the process is taken from the workDirBase option while the location of your clone of this package should be set with the basePath option (which has a default of the current working directory). This is also the place where the number of events per job is set. 

Once the diagrams for your process have been produced, you can now use copyMGOutDri.py to generate the events. This script creates a new output directory for each of process ID and uses the process ID as a random seed for that generation. As much as possible is sym linked rather than copied from the original output directory. There are several options including how many cores to use per job and how many events to produce per node. Pythia8 is automatically run after the Madgraph generation and the outputs are placed in the "tmp/stop_seedStartPID/Events/run_01/" directory, where PID is the job process ID. 

## Setup instructions.

1. Make a working director: mkdir mgTest2

2. Clone the repository: git clone git@gitlab.com:walter.h.hopkins/madgraphbatch.git

3. Source the setup script: cd madgraphbatch; source setup.sh
   This downloads Madgraph, installs it along with Pythia8 and Delphes, and changes the mg5_configuration.txt file to put Madgraph in single core mode.

4. Setup versions gnu version of fortran: module switch PrgEnv-intel PrgEnv-gnu

5. Setup cray python: module load cray-python

6. Make a directory for you logs: mkdir log

## Generating diagrams and events.

There are two steps to generating events: first you must generate the diagrams with generate.py and then you have to generate the events with copyMGOutDir.py

Here are some example command:

python generate.py stop /projects/atlas_aesp/whopkins/mgTest2/

python copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/

## Running on worker nodes.

There is a sample script meant to test the setup called submitTestJobs.sh to submit the jobs to the theta worker nodes. This is where the job can be scaled to any desired amount. To submit the test job simply do the following:

qsub --mode script submitTestJob.sh


## To do

- Currently madgraph recompiles the subprocess directory for each copy of the output directory. I changed this manually in madevent.py inside Madgraph but a better solution should be found. 