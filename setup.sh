cd ../
wget https://launchpad.net/mg5amcnlo/2.0/2.6.x/+download/MG5_aMC_v2.6.4.tar.gz

tar -zxf MG5_aMC_v2.6.4.tar.gz
rm MG5_aMC_v2.6.4.tar.gz

cd MG5_aMC_v2_6_4

printf "\ninstall pythia8\nquit\n"|./bin/mg5_aMC

printf "\ninstall Delphes\nquit\n"|./bin/mg5_aMC

sed -i 's/# automatic_html_opening = True/automatic_html_opening = False/' input/mg5_configuration.txt
sed -i 's/# run_mode = 2/run_mode = 0/' input/mg5_configuration.txt
#sed -i 's/!partonlevel:mpi = off/partonlevel:mpi = off/' Template/LO/Cards/pythia8_card_default.dat

cd ../madgraphbatch
