#!/bin/env python

import math, sys, glob, os, shutil, argparse, fileinput
from processes import procDict
from tempfile import mkstemp

def replace(file_path, patternsSubs):
    #Create temp file
    fh, abs_path = mkstemp()
    with os.fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                newLine = line
                for (pattern, subst) in patternsSubs:
                    newLine = newLine.replace(pattern, subst)
                    
                new_file.write(newLine)
    #Remove original file
    os.remove(file_path)
    #Move new file
    shutil.move(abs_path, file_path)
    
def generate(nEvtsPerNode, process, workDirBase, basePath="../", baseNameSuffix=''):
    """!
    This function generates a process in Madgraph. This function assumes that you have already modified the templates in madgraph so they have your 
    generator filters, center of mass energy, etc. You also have to modify the parameter card with the decays you want to allow and the masses
    You would like for your particles. This script only replaces iseed and nevents in the run_card. 
    """
    # Check if the process is defined in processes.py
    if process not in procDict:
        print "Please add your process to the procDict in processes.py."
        return 1
    
    # Maximum number of events allowed when running with pythia
    maxEvtsPerNode = 50000

    if nEvtsPerNode > maxEvtsPerNode:
        print "You can't run more than 50K events per run. Setting events per node to", maxEvtsPerNode
        nEvtsPerNode = maxEvtsPerNode

    # Write proc_cards and executable
    if baseNameSuffix == '':
        idStr = "%s" % (process)
    else:
        idStr = "%s_%s" % (process, baseNameSuffix)
    
    workDir = workDirBase+"/tmp/"+idStr
    print "workDir=", workDir

    # Write the process to a file and run mg5 with it.
    if not os.path.exists('cards'):
        os.mkdir('cards')

    if os.path.exists(workDir):
        shutil.rmtree(workDir)
    procCardHeader = procDict[process]+"\noutput "+workDir
    procF = open("cards/proc_card_"+idStr+".dat", 'w');
    procF.write(procCardHeader+"\n")
    procF.close();
    os.system("../MG5_aMC_v2_6_4/bin/mg5_aMC cards/proc_card_"+idStr+".dat")

    replace(workDir+"/Cards/run_card.dat", [("10000 = nevents",str(nEvtsPerNode)+" = nevents")])

    # Compile
    os.system("cd "+workDir+"/Source; make")
    print "Done compiling."

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Submit Madgraph jobs to condor')
    parser.add_argument('process', type=str, help='Process to generate', default="stop")
    parser.add_argument('workDirBase', type=str, help='Base working directory; could be local or shared')
    parser.add_argument('--nEvtsPerNode', type=int, help='Number of events each node should process', default=25000)
    parser.add_argument('--basePath', type=str, help='Location where MadGraph is installed', default=os.getcwd())
    parser.add_argument('--baseNameSuffix', type=str, help='Suffix to help make your output directory be unique', default="")
    args = parser.parse_args()

    generate(args.nEvtsPerNode, args.process, args.workDirBase, basePath=args.basePath, baseNameSuffix=args.baseNameSuffix)
