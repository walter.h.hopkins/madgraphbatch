#!/bin/bash
#COBALT -t 60
#COBALT -n 8
#COBALT -A atlas_aesp
#COBALT --jobname madgraphtest
#COBALT -q debug-cache-quad                        
#COBALT --cwd /home/whopkins/mgTest2/madgraphbatch/
module switch PrgEnv-intel PrgEnv-gnu
module load cray-python

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 1 -d 1 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/  --nb_core 1 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_1 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 1 -d 4 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 4 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_2 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 1 -d 8 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 8 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_3 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 1 -d 16 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 16 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_4 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 1 -d 32 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 32 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_5 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 1 -d 64 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 64 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_6 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 2 -d 128 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 128 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_7 2>&1 &

aprun --cc depth --env --cwd=/home/whopkins/mgTest2/madgraphbatch/ -n 1 -N 1 -j 4 -d 256 /home/whopkins/mgTest2/madgraphbatch/copyMGOutDir.py stop /projects/atlas_aesp/whopkins/mgTest2/ --nb_core 256 > /home/whopkins/mgTest2/madgraphbatch/log/cobalt_stdouterr.$COBALT_JOBID.log_8 2>&1 &


wait
