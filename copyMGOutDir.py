#!/usr/bin/python

import math, sys, glob, os, shutil, argparse, fileinput, socket
from tempfile import mkstemp


def copyMG(process, workDirBase, basePath="../", baseNameSuffix='', getIDStrOnly=False, nb_core=2):
    """!
    This function copies the output directory (with as many sym links as possible). The copies of 
    the output directory have slightly different run cards since the random seed needs to be different 
    for each directory to not generate the same events over and over again. This function assumes 
    that you have already modified the templates in madgraph so they have your generator filters, 
    center of mass energy, etc. You also have to modify the parameter card with the decays you 
    want to allow and the masses You would like for your particles. This script only replaces 
    iseed and nevents in the run_card. 
    """
    sys.path.append(basePath)
    from generate import replace

    nodeName = socket.gethostname()
    print basePath, os.getpid(), nodeName

    nodeNum = str(os.getpid())
    # Write proc_cards and executable
    if baseNameSuffix == '':
        idStr = "%s_seedStart%s" % (process, nodeNum)
        origIDStr = "%s" % (process)
    else:
        idStr = "%s_%s_seedStart%s" % (process, baseNameSuffix, nodeNum)
        origIDStr = "%s_%s" % (process, baseNameSuffix)

    # This was originally meant for when you want to interface this with a script that does a scan of parameters.
    if getIDStrOnly:
        return idStr
    
    workDir = workDirBase+"/tmp/"+idStr
    origDir = workDirBase+"/tmp/"+origIDStr
    print "workDir=", workDir

    lnList = ['lib', 'HTML', 'Source', 'bin/internal/ufomodel/*.py']
    # Now copy, delete, sym link, and modify run_cards.dat
    print "Copying", origDir, workDir
    if os.path.exists(workDir):
        shutil.rmtree(workDir)
    shutil.copytree(origDir, workDir)

    for lnPath in lnList:
        if "*" in lnPath:
            for fName in glob.glob(origDir+"/"+lnPath):
                os.remove(workDir+"/"+os.path.dirname(lnPath)+"/"+os.path.basename(fName))
                os.symlink(fName, workDir+"/"+os.path.dirname(lnPath)+"/"+os.path.basename(fName))
        else:
            shutil.rmtree(workDir+"/"+lnPath);
            os.symlink(origDir+"/"+lnPath, workDir+"/"+lnPath)
    replace(workDir+"/Cards/run_card.dat", [("1 = iseed ", nodeNum+" = iseed")])
    os.system("python -O "+workDir+"/bin/generate_events --multicore -f --nb_core="+str(nb_core))

      

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Submit Madgraph jobs to condor')
    parser.add_argument('process', type=str, help='Process to generate', default="stop")
    parser.add_argument('workDirBase', type=str, help='Base working directory.')
    parser.add_argument('--basePath', type=str, help='Location where MadGraph is installed', default=os.getcwd())
    parser.add_argument('--baseNameSuffix', type=str, help='Suffix to help make your output directory be unique', default="")
    parser.add_argument('--nb_core', type=int, help='Number of cores to use', default=2)
    args = parser.parse_args()

    copyMG(args.process, args.workDirBase, basePath=args.basePath, baseNameSuffix=args.baseNameSuffix, nb_core=args.nb_core)
